#!/bin/bash
#SBATCH --job-name=header
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o header_%j.out
#SBATCH -e header_%j.err

python header.py

