#!/bin/bash
#SBATCH --job-name=sickle
#SBATCH -o sickle-%j.output
#SBATCH -e sickle-%j.error
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --mem=10G
#SBATCH --partition=general

module load sickle/1.33

sickle pe -f Gametophyte1_S7_L001_R1_001.fastq -r Gametophyte1_S7_L001_R2_001.fastq -t sanger -o G1-L1-R1.fastq -p G1_L1_R2.fastq -s G1L1_singles.fastq -n -q 25 -l 36
sickle pe -f Gametophyte1_S7_L002_R1_001.fastq -r Gametophyte1_S7_L002_R2_001.fastq -t sanger -o G1-L2-R1.fastq -p G1-L2-R2.fastq -s G1L2-singles.fastq -n -q 25 -l 36
sickle pe -f Gametophyte1_S7_L003_R1_001.fastq -r Gametophyte1_S7_L003_R2_001.fastq -t sanger -o G1-L3-R1.fastq -p G1_L3-R2.fastq -s G1L3_singles.fastq -n -q 25 -l 36
sickle pe -f Gametophyte1_S7_L004_R1_001.fastq -r Gametophyte1_S7_L004_R2_001.fastq -t sanger -o G1_L4_R1.fastq -p G1_L4_R2.fastq -s G1L4_singles.fastq -n -q 25 -l 36
