#!/bin/bash
#SBATCH --job-name=Usearch
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o Usearch_%j.out
#SBATCH -e Usearch_%j.err

module load usearch/9.0.2132
usearch -cluster_fast G1_fpkm_filtered.fasta.transdecoder.cds -id 0.80 -threads 4 -centroids clustered.filtered.fasta -uc clusters.uc


