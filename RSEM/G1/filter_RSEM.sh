#!/bin/bash
#SBATCH --job-name=RSEM-filter
#SBATCH -o RSEM_filter-%j.output
#SBATCH -e RSEM_filter-%j.error
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --mem=10G
#SBATCH --partition=general

module load trinity/2.6.6

/UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/RSEM/G1/cutoff_FPKM0.5/filter_fasta_by_rsem_values.pl \
	--rsem_output=/UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/RSEM/G1/cutoff_FPKM0.5/G1.RSEM.isoforms.results \
	--fasta=/UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/RSEM/G1/G1.Trinity.fa \
	--output=/UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/RSEM/G1/cutoff_FPKM0.5/G1_fpkm_filtered.fasta \
	--fpkm_cutoff=0.5 \
	--isopct_cutoff=1

