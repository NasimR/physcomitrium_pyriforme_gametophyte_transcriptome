#!/bin/bash
#SBATCH --job-name=contam_removal
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=128M
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o contam_removal_%j.out
#SBATCH -e contam_removal_%j.err

python contam_removal.py -c /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/nr.enTAP/no_internal/python_contam_removal/best_hits_contam.faa  -f /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/nr.enTAP/no_internal/python_contam_removal/clustered.filteredHeader.fasta.transdecoder.cds
