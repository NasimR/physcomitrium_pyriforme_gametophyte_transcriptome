#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 29 13:55:01 2018

@author: abedghanbari
"""

with open("genenames.txt") as f:
    content = f.readlines()
content = [x.strip() for x in content] 

content_unique = list(set(content))

print(len(content_unique))
