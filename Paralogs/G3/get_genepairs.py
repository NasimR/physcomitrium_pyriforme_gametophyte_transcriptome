#!/usr/bin/env python

#################################################################################
## Coded by Neranjan V Perera (2017 June 19)					#
## The script will take a pair of gene ids and the fasta file with all sequences#
## and will produce a fasta files with each gene pairs				#
##										#
## Command: python m_fasta.py <gene_pair_list> <fasta file>			#
## eg: python get_genepairs.py gene_ids.txt fasta.transdecoder.cds		#
## eg: python m_fasta.py gene_ids.txt Ppatens_318_v3.3.cds.fa 			#
#################################################################################

import sys, re
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

uc_genepair_file = sys.argv[1]
centroid_fasta_file = sys.argv[2]

def fasta_check(c_file,g):
        count =0
	for seq_record in SeqIO.parse(c_file, "fasta"):
		if (g == seq_record.id):
			print("seq record matched %s : %s" %(g,seq_record.id))
			rec = SeqRecord(seq_record.seq,
				id = seq_record.id,
				description = seq_record.description)
			break

	return(rec);

#check the centroid file 
def check_uc(u_file):
	count=0
	file = open(u_file, 'r')
	for line in file:
		count = count+1
		#split_line=re.split(r' ',line)
		line = line.strip()
		split_line = line.split("\t")
		gene1 = split_line[0]
		gene2 = split_line[1]
		#sent the gene names to the other function to get the fasta sequence
		print "Gene 1: " + gene1 + " Gene 2 : " + gene2
		gene1_1 = split_line[0].split("|")
		gene2_1 = split_line[1].split("|")
		#print  gene2_1[0] 
		r1 = fasta_check(centroid_fasta_file,gene1)
		r2 = fasta_check(centroid_fasta_file,gene2)
		fasta_out = open(gene1_1[0]+"_"+gene2_1[0]+"_"+str(count)+".fasta", "w")
		SeqIO.write(r1, fasta_out, "fasta")
		SeqIO.write(r2, fasta_out, "fasta")
	return;
#gets the gene names and produce the fasta sequence using the centroid file
	#c_file.close()

check_uc(uc_genepair_file)	

