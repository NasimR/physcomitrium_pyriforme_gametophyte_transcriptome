import argparse

_input_tsv = ""
_input_contam = ""
_input_fasta = ""
_contam_dict = {}


def init_argparse():
    global _input_tsv
    global _input_contam
    global _input_fasta
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', action='store', dest='contam', help='Path to contam fasta file', type=str)
    parser.add_argument('-t', action='store', dest='tsv', help='Path to bests hit tsv file', type=str)
    parser.add_argument('-f', action='store', dest='fasta', help='Path to best hit fasta file', type=str)
    args = parser.parse_args()
    _input_contam = args.contam
    _input_tsv = args.tsv
    _input_fasta = args.fasta


def init_contams():
    with open(_input_contam, 'r') as input_file:
        for line in input_file:
            if not line:
                continue
            if line[0] == '>':
                _contam_dict[line[1:line.find("\t")]] = 1


def parse_fasta():
    outpath = _input_fasta + "_no_contam"
    out_file = open(outpath, 'w')
    lines = []
    is_contam = False
    with open(_input_fasta, 'r') as input_file:
        for line in input_file:
            if not line:
                continue
            if line[0] == '>':
                if _contam_dict.has_key(line[1:line.find("\t")]):
                    is_contam = True
                    continue
                is_contam = False
            else:
                if is_contam:
                    continue
            lines.append(line)
    out_file.writelines(lines)
    out_file.close()


def parse_tsv():
    outpath = _input_tsv + "_no_contam"
    out_file = open(outpath, 'w')
    lines = []
    with open(_input_tsv, 'r') as input_file:
        for line in input_file:
            if _contam_dict.has_key(line[0:line.find('\t')]):
                continue
            lines.append(line)
    out_file.writelines(lines)
    out_file.close()


init_argparse()
init_contams()
parse_fasta()
parse_tsv()