#!/bin/bash
#SBATCH -J RSEM
#SBATCH -o RSEM_%j.out
#SBATCH -e RSEM_%j.err
#SBATCH -N 1
#SBATCH --partition=xeon
#SBATCH -n 1
#SBATCH -c 30
#SBATCH --mem=50G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu

module load bowtie2/2.3.3.1
module load rsem/1.3.0
module load perl/5.24.0
export PERL5LIB=/UCHC/LABS/Wegrzyn/WalnutGenomes/scripts/perl5/lib/perl5/

FILE1=/UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G3/cat.G3.R1.fastq
FILE2=/UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G3/cat.G3.R2.fastq

###RSEM1
rsem-prepare-reference -p 30 --bowtie2 G3.Trinity.fa G3.Trinity
rsem-calculate-expression -p 30 --bowtie2 --paired-end $FILE1 $FILE2 G3.Trinity G3.RSEM





