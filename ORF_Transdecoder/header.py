import linecache
filename1="/UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/Transdecoder/clustered.filtered.fasta"

fastanames=[]
fastalines=[]
i=1
for row in open(filename1,'r'):
 if row.startswith(">") == True:
     fastanames.append(row[1:])
     fastalines.append(i)
 i+=1    
fastalines.append(i)

counter=1

with open("/UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/Transdecoder/clustered.filteredHeader.fasta",'w') as xyz:
   for name in fastanames:
       n=name.split(" ")
       k=fastanames.index(name)
       entry1=fastalines[k]
       entry2=fastalines[k+1]
       dnaseq=""
       header=n[0]+"_"+str(counter)+"\n"
       for m in range(entry1+1,entry2):
          dnaseq+=linecache.getline(filename1,m)    
       xyz.write("%s%s%s" %(">",header,dnaseq))
       counter+=1
