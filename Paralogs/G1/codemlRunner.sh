#!/bin/bash
#SBATCH --job-name=codeml
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o codeml_%j.out
#SBATCH -e codeml_%j.err 

module load paml/4.9 

for file in *; do
	if [[ "$file" == *header* ]]
	then
		shortfile=${file/.header/}
		sed -i "1s/.*/seqfile = $file/" codeml.ctl
		sed -i "3s/.*/outfile = $shortfile.results.txt/" codeml.ctl
        	codeml codeml.ctl
	fi
done
