#!/bin/bash
#SBATCH --job-name=star
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o star_%j.out
#SBATCH -e star_%j.err

module load STAR/2.5.3a
STAR --genomeDir /isg/shared/databases/alignerIndex/plant/physcomitrella3.0/Ppatens3.0_STAR/ --readFilesIn /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G1/cat.G1.R1.fastq /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G1/cat.G1.R2.fastq --runThreadN 4 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix physcomitrellamapG1 
STAR --genomeDir /isg/shared/databases/alignerIndex/plant/physcomitrella3.0/Ppatens3.0_STAR/ --readFilesIn /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G2/cat.G2.R1.fastq /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G2/cat.G2.R2.fastq --runThreadN 4 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix physcomitrellamapG2
STAR --genomeDir /isg/shared/databases/alignerIndex/plant/physcomitrella3.0/Ppatens3.0_STAR/ --readFilesIn /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G3/cat.G3.R1.fastq /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G3/cat.G3.R2.fastq --runThreadN 4 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix physcomitrellamapG3


