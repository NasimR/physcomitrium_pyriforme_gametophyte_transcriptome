#!/bin/bash
#SBATCH --job-name=RSEM-filter
#SBATCH -o RSEM_filter-%j.output
#SBATCH -e RSEM_filter-%j.error
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --mem=10G
#SBATCH --partition=general

module load trinity/2.6.6

/UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/RSEM/G3/cutoff_FPKM0.5/filter_fasta_by_rsem_values.pl \
	--rsem_output=/UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/RSEM/G3/cutoff_FPKM0.5/G3.RSEM.isoforms.results \
	--fasta=/UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/RSEM/G3/G3.Trinity.fa \
	--output=/UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/RSEM/G3/cutoff_FPKM0.5/G3_fpkm_filtered.fasta \
	--fpkm_cutoff=0.5 \
	--isopct_cutoff=1

