#!/bin/bash

lst="00 01 02 03 04 05 06 07 08 09"
for each in $lst
do
	echo $each
	sbatch /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/Exonerate_to_Physcomitrella/troubleshoot1/exonerate.sh $each
done



totalRun=50
filen=10
while [ $filen -le $totalRun ]
do
	sbatch /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/Exonerate_to_Physcomitrella/troubleshoot1/exonerate.sh $filen
        echo $filen
	filen=$((filen + 1))
done
