#!/usr/bin/env python

#argument:
#	read_uc_v1_clean_v2_upperLimit.py <cluster.uc>
#
# output: uper-limit-paralog-pairs
#
#	output file consists of two columns, 
#	first column = <simliar-gene-to-centroid>
#	second column = <centroid-gene>
#	<simliar-gene-to-centroid> <centroid-gene>

import sys,re
file = sys.argv[1]
uc_file = open(file,"r")
h={}
centroid = []
rH = r"^H"
rC = r"^C"

'''
Read file and phase it into two files of "Hits" and "Centroid"
'''
def parse():
	uc_file = open(file, "r")
	Hfile = open("H_file.uc","w")
	Cfile = open("C_file.uc","w")
	for line in uc_file:
		if(re.search(rH, line)):
			Hfile.writelines(line)
			#print("H found",line)
		if(re.search(rC, line)):
			Cfile.writelines(line)
			#print("C found",line)
	#uc_file.close()
	#Hfile.close()
	#Cfile.close()

'''
get the hits into a dictonary
'''
def getHits():
	Hfile = open("H_file.uc","r")
	i=1
	for line in Hfile:
		h[i] = line.strip().split("\t")
		i = i + 1

#gets the Centroid Sequences into a list
def getCentroid():
	#Hfile = open("H_file.uc","r")
	Cfile = open("C_file.uc","r")
	#centroid = []
	for line in Cfile:
		line = line.strip()
		split_line = line.split("\t")
		centroid.append(split_line[8])
	#print("Centroid list: %s" %centroid)
	
'''
match the centroid file to the Hits
'''
def Hits2Centroid():
	gID = open("gene_ID_upper_limit.txt","w")
	for c in centroid:
		seq_aligns=[]
		#print("Centroid ::",c)
		len_h = len(h)
		#print("lenght is :: ", len_h)
		hit_list = {}
		for i in range(1, len_h+1):
			#print("\n\n i= %i , length of h= %i , Hit is : %s " %(i,len_h,h[i][9]))
			h_temp = str(h[i][9])
			#print("\t Hit is  h_temp:: %s " %h_temp)
			c_temp = str(c)
			#print("\t Centroid: :%s: ; Hit/Targe: :%s: Query: :%s:" %(c_temp,h_temp,h[i][8]))
			if(h_temp==c_temp and float(h[i][3]) != 100.0):
				#print("\t\t\t Its a Hit")
				#if(h[i][3] != 100.0):
				hit_list[h[i][3]] = h[i][8]
				#print("\n \t A Hit % is found for Centroid % " %(h[i][9],c))
				entry = (h[i][3],h[i][8],h[i][9])
				seq_aligns.append(entry)
			#else:
				#print("\t\t\t NOT ")
		#hit_list.sort()
		#print("identity(Key)= %s" %hit_list.keys())
		#print("query seq = %s" %hit_list.values())
		if seq_aligns:
			seq_aligns.sort(reverse=True)#reverse=False-> give lower limit & True-> gives upper limit
			print("sorted list: %s %s %s %s" %(seq_aligns[0][1],seq_aligns[0][2],seq_aligns[0][0],len(seq_aligns)))
			gID.writelines(seq_aligns[0][1] + "\t" + seq_aligns[0][2] + "\n")

'''
for line in uc_file:
	m = re.search(regex, line)
	#print m.group()
	if m:
		print m.group()
		print line
		line = line.strip()
		split_line = line.split("\t")
		#print(len(split_line))
		query = split_line[8]
		target = split_line[9]
		print("query: %s Target: %s" %(query,target))
		d[split_line[9]] = split_line[8]
	else:
		print("No match")

	#line = line.strip()
	#split_line = line.split(" ")
	#d[i] = line
	#i = i+1
'''
def main():
	parse()
	getHits()
	getCentroid()
	Hits2Centroid()
	#print("\n\nLenght of Dic: %s " %(len(d)))
	#print("\n Keys: %s" %d.keys())
	#print("\n Values: %s" %h.values())
	#print(d[1])

main()

