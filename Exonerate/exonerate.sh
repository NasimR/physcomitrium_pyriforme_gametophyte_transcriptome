#!/bin/bash
#BATCH -J exonerate

#SBATCH -N 1

#SBATCH -n 1

#SBATCH -c 1

#SBATCH --mail-user=nasim.rahmatpour@uconn.edu

#SBATCH --mail-type=ALL

#SBATCH -o exonearate-%j.output

#SBATCH -e exonerate-%j.error

#SBATCH -p general

#SBATCH --qos=general

#SBATCH --mem=50G

module load exonerate/2.4.0

exonerate --query /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/Exonerate_to_Physcomitrella/splitfasta/Exonerate_chunk_00000"$1" --querytype protein --target /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/Exonerate_to_Physcomitrella/Ppatens_318_v3.softmasked.fa --targettype dna --model protein2genome --percent 70 --score 500 -n 1 --minintron 10  --verbose 1 > /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/Exonerate_to_Physcomitrella/troubleshoot1/output/Physcomitrium_"$1".txt 

if [ $? -eq 0 ]; then
	echo "Exonerate_chunk_00000$1" $1  SUCCESSFUL >> file_status_OK.txt 
else
	echo "Exonerate_chunk_00000$1" $1  FAIL >> file_status_FAIL.txt
fi
