#!/bin/bash
#SBATCH --job-name=get_genepairsh
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o get_genepairs_%j.out
#SBATCH -e get_genepairs_%j.err

module load biopython/1.70
python get_genepairs.py gene_ID_upper_limit.txt G2_fpkm_filtered.fasta.transdecoder.cds
