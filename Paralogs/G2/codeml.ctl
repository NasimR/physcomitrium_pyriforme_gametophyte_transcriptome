seqfile = TRINITY_DN9999_c0_g1__TRINITY_DN9999_c0_g1_i1__g.26736__m.26736_TRINITY_DN9999_c0_g1__TRINITY_DN9999_c0_g1_i2__g.26737__m.26737_1989.fasta.afa.header
treefile = treefile.txt
outfile = TRINITY_DN9999_c0_g1__TRINITY_DN9999_c0_g1_i1__g.26736__m.26736_TRINITY_DN9999_c0_g1__TRINITY_DN9999_c0_g1_i2__g.26737__m.26737_1989.fasta.afa.results.txt

noisy = 9
verbose = 1      * 1:detailed output
runmode = -2     * -2:pairwise
seqtype = 1      * 1:codons
CodonFreq = 0      * 0:equal, 1:F1X4, 2:F3X4, 3:F61
model = 0      *
NSsites = 0      *
icode = 0      * 0:universal code
fix_kappa = 1      * 1:kappa fixed, 0:kappa to be estimated
kappa = 1      * initial or fixed kappa
fix_omega = 0      * 1:omega fixed, 0:omega to be estimated
omega = 0.5    * initial omega value
