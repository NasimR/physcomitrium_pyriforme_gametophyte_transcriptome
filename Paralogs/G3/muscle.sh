#!/bin/bash
#SBATCH --job-name=muscle
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o muscle_%j.out
#SBATCH -e muscle_%j.err

module load muscle/3.8.31
python muscle.py

