#!/bin/bash
#SBATCH --job-name=star
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o star_%j.out
#SBATCH -e star_%j.err

module load STAR/2.5.3a
STAR --genomeDir /UCHC/LABS/Wegrzyn/Funaria_Genome/Funaria_star_index/ --readFilesIn /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G1/cat.G1.R1.fastq /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G1/cat.G1.R2.fastq --runThreadN 4 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix FunariamapG1 
STAR --genomeDir /UCHC/LABS/Wegrzyn/Funaria_Genome/Funaria_star_index/ --readFilesIn /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G2/cat.G2.R1.fastq /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G2/cat.G2.R2.fastq --runThreadN 4 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix FunariamapG2
STAR --genomeDir /UCHC/LABS/Wegrzyn/Funaria_Genome/Funaria_star_index/ --readFilesIn /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G3/cat.G3.R1.fastq /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/catfiles/cat.G3/cat.G3.R2.fastq --runThreadN 4 --outSAMtype BAM SortedByCoordinate --outFileNamePrefix FunariamapG3


