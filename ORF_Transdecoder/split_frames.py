import argparse

_input_path = ""

def init_argparse():
    global _input_path
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', action='store', dest='input', help='Path to input file', type=str)
    args = parser.parse_args()
    _input_path = args.input


def format_seq():
    # read into memory (not to large)
    outpath = _input_path + "_alt"
    out_internal_path = _input_path + "_internals"
    out_complete_path = _input_path + "_completes"
    out_partial_path = _input_path + "_partials"

    out_file = open(outpath, 'w')   # No internal
    out_internal = open(out_internal_path, 'w')
    out_complete = open(out_complete_path, 'w')
    out_partial = open(out_partial_path, 'w')

    partial_list = []
    complete_list = []
    internal_list = []

    list_map = {
        "5prime_partial":partial_list,
        "3prime_partial":partial_list,
        "complete":complete_list,
        "internal":internal_list
    }
    flag = ""
    with open(_input_path, 'r') as input_file:
        for line in input_file:
            if not line:
                continue
            if line[0] == '>':
                temp = line.find("type:")
                flag = line[temp+5:line.find(" ", temp)]
                line = line[0:line.find(" ")]
                line += "\n"
            list_map[flag].append(line)

    out_file.writelines(list_map["complete"])
    out_file.writelines(list_map["5prime_partial"])
    out_complete.writelines(list_map["complete"])
    out_internal.writelines(list_map["internal"])
    out_partial.writelines(list_map["5prime_partial"])

    out_file.close()
    out_internal.close()
    out_complete.close()
    out_partial.close()

init_argparse()
format_seq()
