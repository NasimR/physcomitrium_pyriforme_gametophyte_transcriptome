#!/bin/bash
# Submission script for Xanadu
####SBATCH --time=10-01:00:00 # days-hh:mm:ss
####SBATCH --mem=350GB
#SBATCH --job-name=gth
#SBATCH -o gth-%j.output
#SBATCH -e gth-%j.error
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --qos=general
###SBATCH --array=1-9%9


module load genomethreader/1.6.6

cd /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/GenomeThreader


gth -genomic /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/GenomeThreader/Ppatens_318_v3.softmasked.fa -protein /UCHC/LABS/Wegrzyn/Physcomitrium_transcriptome/Gametophyte/GenomeThreader/Physcomitrium_pyriforme.fasta -gff3out -intermediate 


#------------------------
