#!/bin/bash
#SBATCH --job-name=conservation_folders
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o conservation_folders_%j.out
#SBATCH -e conservation_folders_%j.err

python conservation_folders2.py 3.5 conservation_value_3p5
