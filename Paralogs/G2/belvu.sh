#!/bin/bash
#SBATCH --job-name=belvu
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o belvu_%j.out
#SBATCH -e belvu_%j.err

module load seqtools/4.42.1
python belvu.py

